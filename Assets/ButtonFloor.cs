﻿using UnityEngine;
using System.Collections;

public abstract class ButtonFloor : MonoBehaviour {

    public Animator liftAnim;
    public int floor;

    internal virtual void Init()
    {
        liftAnim = LiftManager.instance.liftAnim;
    }

    void Start()
    {
        Init();
    }
    

    void OnTriggerEnter(Collider other)    
    {
        LiftManager.instance.CallLift(floor);
        LiftManager.instance.fotokomCrosed = false;
    }
}
