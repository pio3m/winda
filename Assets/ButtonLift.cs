﻿using UnityEngine;
using System.Collections;

public class ButtonLift : MonoBehaviour {

    public Animator liftAnima;
    // Use this for initialization
    void Start()
    {
        liftAnima = LiftManager.instance.liftAnim;
    }

    void OnTriggerEnter(Collider other)
    {
        //drow gui
        LiftManager.instance.toggleGUI = true;
        LiftManager.instance.fotokomCrosed = false;
    }

}
