﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    public float speed;

    void Update()
    {
        if (Input.GetAxis("Vertical") != 0)
        {
            transform.Translate(new Vector3(0, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime));
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            transform.Translate(new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0,0));
        }     
    }
}