﻿using UnityEngine;
using System.Collections;

public class LiftManager : MonoBehaviour {

    public static LiftManager instance;
    public GameObject player;
    public Animator liftAnim;
    public int floorActual;
    public int floorGoTo;
    public int speed;
    //door
    public bool doorClosed;
    public bool fotokomCrosed;
    AnimatorStateInfo currentState;
    //GUI
    public bool toggleGUI;
    AudioSource audioLift;

    void Awake() {
        if (instance == null)
            instance = this;
    }

	void Start () {
       
        floorActual = 0;
        floorGoTo = 0;
        toggleGUI = false;
        doorClosed = true;
        fotokomCrosed = false;
        audioLift = GetComponent<AudioSource>();
    }
	
    void Update () {
	    if(floorGoTo != floorActual)
        {
            CallLift(floorGoTo);
        }

    }
    public void SetFloorGoTo(int floor)
    {
      
        CheckIfDoorClosed();

        if (doorClosed)
        {                      
            floorGoTo = floor;
        }
        else
        {
            StartCoroutine(WaitUntilDoorClosed(floor));
        }       
    }


    void OnGUI()
    {
        if (toggleGUI == true)
        {            

            if (GUI.Button(new Rect(10, 70, 50, 30), "Floor 1"))
            {
                player.transform.parent = transform;
                SetLiftDestinationFloor(0);
            }

            if (GUI.Button(new Rect(10, 100, 50, 30), "Floor 2"))
            {
                player.transform.parent = transform;
                SetLiftDestinationFloor(1);
            }

            if (GUI.Button(new Rect(10, 130, 50, 30), "Floor 3"))
            {
                player.transform.parent = transform;
                SetLiftDestinationFloor(2);
            }

        }
    }

    void CheckIfDoorClosed()
    {
        if (liftAnim.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            doorClosed = true;            
        }else
        {
            doorClosed = false;
        }
    }

    void LiftArrive()
    {
        audioLift.Stop();
        floorActual = floorGoTo;
        toggleGUI = false;
        player.transform.SetParent(null);
        liftAnim.SetTrigger("Open");      
        doorClosed = false;
    }

    void LiftGoingUp()
    {
        if(!audioLift.isPlaying)
            audioLift.Play();

        transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        if(transform.position.y >= floorGoTo * 10)
        {
            LiftArrive();
        }
    }

    void LiftGoingDown()
    {
        if (!audioLift.isPlaying)
            audioLift.Play();

        transform.Translate(new Vector3(0, - speed * Time.deltaTime, 0));
        if (transform.position.y <= floorGoTo * 10)
        {
            LiftArrive();
        }
    }

    public void CallLift(int floor)
    {
        if(floorActual == floor)
        {
            // LiftArrive();
            if (doorClosed)
            {
                liftAnim.SetTrigger("Open");
                doorClosed = false;
            }

        }
        else if(floorActual < floor )
        {
            SetFloorGoTo(floor);
            LiftGoingUp();
        } else
        {
            SetFloorGoTo(floor);
            LiftGoingDown();
        }
    } 

    void SetLiftDestinationFloor(int floor)
    {
        toggleGUI = false;
       
        if (floorActual == floor)
        {
            //To samo piętro
        }else if(liftAnim.GetFloat("speed") < 0)
        {
            liftAnim.SetFloat("speed", 1);
            liftAnim.Play("otwieranie", 0, 1f);
            liftAnim.SetTrigger("Close");
            StartCoroutine(CloseDoor(floor));            
        }
        else
        {
            liftAnim.SetTrigger("Close");
            StartCoroutine(CloseDoor(floor));
        }

    }

    IEnumerator CloseDoor(int floor)
    {             
        yield return new WaitForSeconds(3);
        if (!fotokomCrosed)
        {
            liftAnim.SetBool("doorClosed", true);
            SetFloorGoTo(floor);

        }
    }


    IEnumerator WaitUntilDoorClosed(int floor)
    {

        yield return new WaitForSeconds(3);
        if (!fotokomCrosed)
            SetFloorGoTo(floor);

    }

}
