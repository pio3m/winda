﻿using UnityEngine;
using System.Collections;

public class Fotokom : MonoBehaviour {

   
	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Move>())
        {
            LiftManager.instance.toggleGUI = false;

            LiftManager.instance.fotokomCrosed = true;

            if (LiftManager.instance.liftAnim.GetCurrentAnimatorStateInfo(0).IsName("zamykanie"))
            {
                LiftManager.instance.liftAnim.SetFloat("speed", -1);            
            }
                        
        }
    }
    
}
